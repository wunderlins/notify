//
//  ViewController.swift
//  notify
//
//  Created by Simon Wunderlin on 23.09.20.
//  Copyright © 2020 Simon Wunderlin. All rights reserved.
//

import UIKit

struct ListItem : Codable {
	var id: Int
	var title: String
}

class ViewController: UIViewController, UITableViewDelegate,  UITableViewDataSource {
	
	var items = [ListItem]()
	
	let spinner = UIActivityIndicatorView(style: .large)
	var initialized = false
	
	let refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
                     #selector(ViewController.handleRefresh(_:)),
								 for: UIControl.Event.valueChanged)
		//refreshControl.tintColor = UIColor.red
        
        return refreshControl
    }()

	@objc func handleRefresh(_ refreshControl: UIRefreshControl) {
		debugPrint("Reloading ...")
        self.loadData()
    }
	
    let tableview: UITableView = {
        let tv = UITableView()
        tv.backgroundColor = UIColor.white
        tv.translatesAutoresizingMaskIntoConstraints = false
        tv.separatorColor = UIColor.white
		
		//tv.addSubview(self.refreshControl)
		return tv
    }()
	
	required init?(coder: NSCoder) {
		super.init(coder: coder)
		// fatalError("init(coder:) has not been implemented")
		tableview.addSubview(self.refreshControl)
	}
	
	func loadData() {
		
		tableview.backgroundView = spinner
		if (initialized == false) {
			spinner.startAnimating()
		}
		
		let url = URL(string: "http://localhost:9001/rand")!
		let urlSession = URLSession.shared
		debugPrint("Fetching: ", url)
		let task = urlSession.dataTask(with: url) {(data, response, error) in
			OperationQueue.main.addOperation {
				self.refreshControl.endRefreshing()
				self.spinner.stopAnimating()
				self.initialized = true
			}
			
			// Fehlerbehandlung für den Fall, das ein Fehler aufgetreten ist und data nicht gesetzt ist
			guard let data = data else {
				debugPrint("Fehler beim Laden", error ?? "Unbekannter Fehler")
				return
			}

			// JSON parsen
			self.items = try! JSONDecoder().decode([ListItem].self, from: data)
			//self.tableview.dataSource = self.items
			
			// UI-Darstellung aktualisieren
			OperationQueue.main.addOperation {
				self.tableview.reloadData()
			}

		}
		task.resume()
		
	}
	
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.
		
		setupTableView()
		loadData()
			
	}

	func setupTableView() {
		
        tableview.delegate = self
        tableview.dataSource = self
		
		//tableview.register(UITableViewCell.self, forCellReuseIdentifier: "cellId")
		tableview.register(ThirtyDayCell.self, forCellReuseIdentifier: "cellId")
		
		view.addSubview(tableview)
		
		NSLayoutConstraint.activate([
			tableview.topAnchor.constraint(equalTo: self.view.topAnchor),
			tableview.bottomAnchor.constraint(equalTo: self.view.bottomAnchor),
			tableview.rightAnchor.constraint(equalTo: self.view.rightAnchor),
			tableview.leftAnchor.constraint(equalTo: self.view.leftAnchor)
		])
	}
	
	// return list length
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return self.items.count
    }
	
	// cell height
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return 100
	}
	
	// render cell ?
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableview.dequeueReusableCell(withIdentifier: "cellId", for: indexPath) as! ThirtyDayCell
		cell.backgroundColor = UIColor.white
		cell.dayLabel.text = items[indexPath.row].title // "Day \(indexPath.row+1)"
		
		return cell
	}
	
	// swipe actions
	func tableView(_ tableView: UITableView,
		leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {

		/*
		// Get current state from data source
		guard let favorite = items[indexPath.row] else {
		return nil
		}

		let title = favorite ?
		NSLocalizedString("Unfavorite", comment: "Unfavorite") :
		NSLocalizedString("Favorite", comment: "Favorite")
		*/
		let action = UIContextualAction(style: .destructive, title: title,
			handler: { (action, view, completionHandler) in
			// Update data source when user taps action
			debugPrint("Delete Item action: id: ", self.items[indexPath.row].id)
			
			// TODO: send rest call
				
			// remove item
			
			OperationQueue.main.addOperation {
				self.items.remove(at: indexPath.row)
				self.tableview.deleteRows(at: [indexPath], with: .automatic)
				debugPrint("Items length: ", self.items.count)
			}
			
			// set false on error
			completionHandler(true)
		})

		action.image = UIImage(systemName: "trash")
		//action.title = "Done"
		//action.backgroundColor = favorite ? .red : .green
		let configuration = UISwipeActionsConfiguration(actions: [action])
		
		// disable swipe delete, useful if there are more actions than delete
		//configuration.performsFirstActionWithFullSwipe = false
		
		return configuration
	}
}

class ThirtyDayCell: UITableViewCell {
    let cellView: UIView = {
        let view = UIView()
        //view.backgroundColor = UIColor.red
        view.layer.cornerRadius = 10
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let dayLabel: UILabel = {
        let label = UILabel()
        label.text = "---"
        //label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
	
	override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
		super.init(style: style, reuseIdentifier: reuseIdentifier)
		setupView()
	}

	func setupView() {
		addSubview(cellView)
		cellView.addSubview(dayLabel)
		//cellView.addSubview(dayLabel2)
		self.selectionStyle = .none
		
		NSLayoutConstraint.activate([
			cellView.topAnchor.constraint(equalTo: self.topAnchor, constant: 20),
			cellView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -10),
			cellView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 10),
			cellView.bottomAnchor.constraint(equalTo: self.bottomAnchor)
		])
		
		dayLabel.heightAnchor.constraint(equalToConstant: 200).isActive = true
		dayLabel.widthAnchor.constraint(equalToConstant: 200).isActive = true
		dayLabel.centerYAnchor.constraint(equalTo: cellView.centerYAnchor).isActive = true
		dayLabel.leftAnchor.constraint(equalTo: cellView.leftAnchor, constant: 20).isActive = true
		
	}
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
