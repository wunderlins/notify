//
//  AppDelegate.swift
//  notify
//
//  Created by Simon Wunderlin on 23.09.20.
//  Copyright © 2020 Simon Wunderlin. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

	// enable push notifications
	func registerForPushNotifications(application: UIApplication) {
		let notificationSettings = UIUserNotificationSettings(
			types: [.badge, .sound, .alert], categories: nil)
		application.registerUserNotificationSettings(notificationSettings)
	}
	
	func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
		// Override point for customization after application launch.
		registerForPushNotifications(application: application)
	 return true
	}
	
	func application(application: UIApplication, didRegisterUserNotificationSettings notificationSettings: UIUserNotificationSettings) {
	  if notificationSettings.types != [] {
		application.registerForRemoteNotifications()
	 }
	}
	
	func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
	  // let tokenChars = UnsafePointer<CChar>(deviceToken.bytes)
	  let tokenChars = deviceToken.map { String(format: "%02.2hhx", $0) }.joined()
		/*
	  var tokenString = ""
	  
	  for i in 0..<deviceToken.length {
		tokenString += String(format: "%02.2hhx", arguments:[tokenChars[i]])
	  }
	  */
	  print("Device Token:", tokenChars)
	}
	
	func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError) {
	  print("Failed to register:", error)
	}

	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
		// Override point for customization after application launch.
		return true
	}

	// MARK: UISceneSession Lifecycle

	func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
		// Called when a new scene session is being created.
		// Use this method to select a configuration to create the new scene with.
		return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
	}

	func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
		// Called when the user discards a scene session.
		// If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
		// Use this method to release any resources that were specific to the discarded scenes, as they will not return.
	}


}

