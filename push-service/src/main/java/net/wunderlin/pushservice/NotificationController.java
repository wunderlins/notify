package net.wunderlin.pushservice;

import java.util.Map;
import java.util.Date;

import java.util.concurrent.atomic.AtomicLong;

import javax.management.Notification;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.notnoop.apns.APNS; 
import com.notnoop.apns.ApnsService;

/**
 *
 * @author victorleungtw
 */
@RestController
public class NotificationController {

    private static final String template = "Hello, %s!";
	private final AtomicLong counter = new AtomicLong();
	ApnsService service = null;

	@RequestMapping("/notification")
	public Boolean notification(@RequestParam(value="name", defaultValue="World") String name) {

		System.out.println("Sending an iOS push notification…");

		ApnsService service = APNS.newService()
            .withCert("../resources/aps_development.p12", "test1234")
            .withSandboxDestination()
            .build(); 

		String payload = APNS.newPayload()
            .alertBody("Message")
            .alertTitle("Title").build();

		String token = "b8f3b2513caa";

		System.out.println("payload: "+payload);

		service.push(token, payload);

		System.out.println("The message has been hopefully sent…");

		//return new Notification(counter.incrementAndGet(), String.format(template, name));
		return true;
	}
}