package net.wunderlin.pushservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Map; 
import java.util.Date;
import java.util.concurrent.atomic.AtomicLong;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.notnoop.apns.APNS; 
import com.notnoop.apns.ApnsService;

@SpringBootApplication
public class PushServiceApplication {



	public static void main(String[] args) {
		SpringApplication.run(PushServiceApplication.class, args);
	}

}
